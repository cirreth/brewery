#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include "indexhtml.h"
#include "pics.h"

#ifndef STASSID
#define STASSID "ASUS-home"
#define STAPSK  "pass0pass0x"
#define OTApassword "pass" //the password you will need to enter to upload remotely via the ArduinoIDE
#define OTAport 8266
#define deviceName "brewery"
#define ONE_WIRE_BUS 2
#define HEATER_LED_PIN 16
#define HEATER_PIN 14
#define ZERO_DETECTOR_PIN 12
#define ENABLE_LED_FEEDBACK LED_BUILTIN
#endif


#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);


const char* ssid = STASSID;
const char* password = STAPSK;

WiFiServer server(80);
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress Thermometer;
int deviceCount = 0;
char temperatureCString[6];
float temp0 = 0.0;
long tempPrevRead = 0;
long displayPrevRenew = 0;
int heaterPwmValue = 0;
int manualHeaterPwmValue = 0;
volatile int zeroCrossNumber = 0;
int requestedTemp = 0;

// прерывание детектора нуля
void ICACHE_RAM_ATTR isr() {
  zeroCrossNumber++;
  if (zeroCrossNumber > 49) zeroCrossNumber = 0;
  int value = (zeroCrossNumber*2 < heaterPwmValue) ? 1 : 0;
  digitalWrite(HEATER_PIN, value);
  digitalWrite(HEATER_LED_PIN, value);
}


void setup() {

  ////
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { 
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }

  display.clearDisplay();
  display.drawBitmap(0, 0, epd_bitmap_beer, 128, 64, 1);
  display.display();
  delay(1000);

  ////
  
  // prepare LED
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, 0);
  pinMode(HEATER_PIN, OUTPUT);
  digitalWrite(HEATER_PIN, 0);
  pinMode(HEATER_LED_PIN, OUTPUT);
  digitalWrite(HEATER_LED_PIN, 0);
  pinMode(ZERO_DETECTOR_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(ZERO_DETECTOR_PIN), isr, FALLING);
  //
  Serial.begin(115200);
  Serial.println("Booting");


///
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,1);
  display.println("Connecting to");
  display.println(STASSID);
  display.display();
///
  
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_FS
      type = "filesystem";
    }

    // NOTE: if updating FS this would be the place to unmount FS using FS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });

  ArduinoOTA.setPort(OTAport);
  ArduinoOTA.setHostname(deviceName); // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setPassword((const char *)OTApassword); // No authentication by default
  ArduinoOTA.begin();
  
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

   // Start the server
  server.begin();
  Serial.println(F("Server started"));

  // Start up the library
  sensors.begin();

  sensors.getAddress(Thermometer, 0);
  printAddress(Thermometer);

}

void loop() {

  ArduinoOTA.handle();

 // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {

    ///
    if (millis() - tempPrevRead > 3000) {
      
      tempPrevRead = millis();

      temp0 = getTemperature(0);
      
    }

    if (millis() - displayPrevRenew > 1000) {

      displayPrevRenew = millis();
        
      display.clearDisplay();
      display.setTextColor(WHITE);
      display.setTextSize(1);
      display.setCursor(3,1);
      display.print(WiFi.localIP());
      display.println("/ui");
  
      display.setCursor(3,16);
      display.print(String(millis()/1000));
      display.print(" (");
      display.print(String(millis()/60000));
      display.print(" min)");
  
      display.setCursor(3, 28);
      display.setTextSize(2);
      display.print(String(temp0));
      
      display.setCursor(80, 28);
      display.println(String(heaterPwmValue));

      if (requestedTemp != 0) {
        display.setTextSize(1);
        display.setCursor(3, 45); 
        display.print("req: ");
        display.println(String(requestedTemp));
      }
          
      display.display();
    }
    
    ///

    if (requestedTemp == 0) {
      heaterPwmValue = manualHeaterPwmValue;
    } else {
       if (temp0 >= requestedTemp) {
          heaterPwmValue = 0;
       } else {
          heaterPwmValue = 100;
          if (requestedTemp - temp0 < 3) heaterPwmValue = 50;
          if (requestedTemp - temp0 < 2) heaterPwmValue = 30;
          if (requestedTemp - temp0 < 1) heaterPwmValue = 15;
       }
    }
    
    return;
  }
  Serial.println(F("new client"));

  client.setTimeout(5000); // default is 1000

  // Read the first line of the request
  String req = client.readStringUntil('\r');
  Serial.println(F("request: "));
  Serial.println(req);

  // Match the request
  int val;
  if (req.indexOf(F("/pwm/")) != -1) {
    int startArgumentIdx = req.indexOf(F("/pwm/"))+String("/pwm/").length();
    int lastArgumentIdx = req.indexOf(" ", startArgumentIdx)+1;
    String valuePart = req.substring(startArgumentIdx, lastArgumentIdx);
    char valueBuffer[4] = {0, 0, 0, 0};
    valuePart.toCharArray(valueBuffer, valuePart.length());
    int value = atoi(valueBuffer);
    manualHeaterPwmValue = value;
    client.printf("HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nContent-Length:%i\r\n\r\n", valuePart.length());
    client.printf("%i", value);
    client.print(F("\r\n"));
    return;
  } else if (req.indexOf(F("/temp/0")) != -1) {
    client.print(F("HTTP/1.1 200 OK\r\nContent-Type: application/json\r\nContent-Length:5\r\n\r\n"));
    client.print(String(temp0));
    return;
  } else if (req.indexOf(F("/pwmstate")) != -1) {
    String value = String(heaterPwmValue);
    client.print(F("HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nContent-Length:"));
    client.print(value.length());
    client.print(F("\r\n\r\n"));
    client.print(value);
    client.print(F("\r\n"));
    return;
  } else if (req.indexOf(F("/reqtemp/")) != -1) {
    int startArgumentIdx = req.indexOf(F("/reqtemp/"))+String("/reqtemp/").length();
    int lastArgumentIdx = req.indexOf(" ", startArgumentIdx)+1;
    String valuePart = req.substring(startArgumentIdx, lastArgumentIdx);
    char valueBuffer[4] = {0, 0, 0, 0};
    valuePart.toCharArray(valueBuffer, valuePart.length());
    int value = atoi(valueBuffer);
    requestedTemp = value;
    client.printf("HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nContent-Length:%i\r\n\r\n", valuePart.length());
    client.printf("%i", value);
    client.print(F("\r\n"));
  } else if (req.indexOf(F("/ui")) != -1) {
    String data = String(index_html);
    client.print(F("HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n"));
    client.print(data);
  } else {
    Serial.println(F("invalid request"));
    val = digitalRead(HEATER_PIN);
    return;
  }

  // Set LED according to the request
  digitalWrite(HEATER_LED_PIN, val);
  digitalWrite(HEATER_PIN, val);

  // read/ignore the rest of the request
  // do not client.flush(): it is for output only, see below
  while (client.available()) {
    // byte by byte is not very efficient
    client.read();
  }

}

void printAddress(DeviceAddress deviceAddress)
{ 
  for (uint8_t i = 0; i < 8; i++)
  {
    Serial.print("0x");
    if (deviceAddress[i] < 0x10) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
    if (i < 7) Serial.print(", ");
  }
  Serial.println("");
}

float getTemperature(int idx) {

  float tempC;

  do {

    sensors.requestTemperatures(); 

    tempC = sensors.getTempCByIndex(idx);

  } while (tempC == 85.0 || tempC == (-127.0));

  return tempC;

}


String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

const char index_html[] PROGMEM = R"rawliteral(
 <!DOCTYPE html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <title>Brewery</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/12.0.0/nouislider.min.js" integrity="sha512-6vo59lZMHB6GgEySnojEnfhnugP7LR4qm6akxptNOw/KW+i9o9MK4Gaia8f/eJATjAzCkgN3CWlIHWbVi2twpg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/12.0.0/nouislider.css" integrity="sha512-de3hHhaaVjGo+KGk523A/Z0k6cgWD3mLLgucg6vSnrdUcDHVhUC2R6PSsgZR6LJ5NjcGPv3IoC1psoY+QILIgA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wnumb/1.2.0/wNumb.min.js" integrity="sha512-igVQ7hyQVijOUlfg3OmcTZLwYJIBXU63xL9RC12xBHNpmGJAktDnzl9Iw0J4yrSaQtDxTTVlwhY730vphoVqJQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> 
    </head>
  </head>
  <body>
    <div class="container center-align" style="user-select: none;">
         <h1>Brewery 0.6</h1>
         <div>
            <i class="large material-icons" style="font-size: 30pt; color: orange">device_thermostat</i>
            <div style="font-size: 20pt"><span id="temp0">?</span> °C (<span id="clock"></span>)</div>
         </div>
        <!-- --> 
        <div>
            <h4>Heater power</h4>
            <div class="col s2 offset-s3"><div id="heater-slider"></div></div>
        </div>
    </div>
  </body>
  <script>

    var pipsSlider = document.getElementById('heater-slider');

    noUiSlider.create(pipsSlider, {
        start: [0],
        step: 10,
        range: {
            'min': 0,
            'max': 100
        },
        pips: {
            mode: 'steps',
            density: 3,
            format: wNumb({
                decimals: 0,
                postfix: '%'
            })
        }
    });

    pipsSlider.noUiSlider.on('set.one', function (values) { 
       console.log('set ', values);
       setHeaterPwm(parseInt(values[0]));
    });
    
    
    function reloadTemp() {
        $.get("/temp/0").done(function(data) {
            console.log(data);
            $('#temp0').html(data);
            showTime();
        });
    }

    
    function reloadPwm() {
        $.get("/pwmstate").done(function(data) {
            console.log(data);
            pipsSlider.noUiSlider.set([data]);
        });
    }

    function setHeaterPwm(value) {
        $.get("/pwm/"+value).done(function(data) {});
    }
    
    function showTime(){
        var date = new Date();
        var h = date.getHours(); // 0 - 23
        var m = date.getMinutes(); // 0 - 59
        var s = date.getSeconds(); // 0 - 59

        h = (h < 10) ? "0" + h : h;
        m = (m < 10) ? "0" + m : m;
        s = (s < 10) ? "0" + s : s;
        
        var time = h + ":" + m + ":" + s;
        $("#clock").html(time);
        
    }

      showTime();
      reloadTemp();
      reloadPwm();
      setInterval(reloadTemp, 5000);
    

  </script>
</html>
)rawliteral";
